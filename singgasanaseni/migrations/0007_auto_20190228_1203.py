# Generated by Django 2.1.4 on 2019-02-28 12:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('singgasanaseni', '0006_auto_20190227_1037'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perupa',
            name='Tanggal_Lahir',
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
        migrations.AlterField(
            model_name='perupa',
            name='Tanggal_Wafat',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
