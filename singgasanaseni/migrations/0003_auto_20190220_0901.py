# Generated by Django 2.1.4 on 2019-02-20 09:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('singgasanaseni', '0002_auto_20190218_0904'),
    ]

    operations = [
        migrations.AlterField(
            model_name='berita',
            name='Isiberita',
            field=models.TextField(blank=True, default=True, null=True),
        ),
        migrations.AlterField(
            model_name='berita',
            name='Link',
            field=models.TextField(blank=True, default=True, null=True),
        ),
        migrations.AlterField(
            model_name='berita',
            name='Sumber',
            field=models.CharField(blank=True, default=True, max_length=30, null=True),
        ),
    ]
