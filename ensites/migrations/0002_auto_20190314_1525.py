# Generated by Django 2.1.4 on 2019-03-14 15:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ensites', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='news',
            options={'verbose_name_plural': 'News in english'},
        ),
    ]
